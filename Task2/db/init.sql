CREATE TABLE students (
   id SERIAL PRIMARY KEY,
   first_name VARCHAR(100),
   last_name VARCHAR(100),
   specialty VARCHAR(100),
   average_grade DECIMAL(3,2)
);

CREATE TABLE subjects (
   id SERIAL PRIMARY KEY,
   subject_name VARCHAR(100),
   teacher_name VARCHAR(100)
);

CREATE TABLE exam_schedule (
   id SERIAL PRIMARY KEY,
   exam_date DATE,
   exam_time TIME,
   student_id INT,
   subject_id INT,
   FOREIGN KEY (student_id) REFERENCES students(id),
   FOREIGN KEY (subject_id) REFERENCES subjects(id)
);

INSERT INTO students (first_name, last_name, specialty, average_grade) 
VALUES 
('Иван', 'Иванов', 'Математика', 4.5),
('Петр', 'Петров', 'Физика', 4.7),
('Сергей', 'Сергеев', 'Химия', 4.8),
('Анна', 'Аннова', 'Биология', 4.9);

INSERT INTO subjects (subject_name, teacher_name) 
VALUES 
('Математика', 'Профессор Матвеева'),
('Физика', 'Доцент Физичева'),
('Химия', 'Доцент Химичева'),
('Биология', 'Профессор Биологова');

INSERT INTO exam_schedule (exam_date, exam_time, student_id, subject_id) 
VALUES 
('2024-01-01', '10:00', 1, 1),
('2024-01-02', '14:00', 2, 2),
('2024-01-03', '10:00', 3, 3),
('2024-01-04', '14:00', 4, 4);
