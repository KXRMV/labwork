import psycopg2

# Подключение к базе данных
conn = psycopg2.connect(
    dbname="students",
    user="postgres",
    password="postgres",
    host="postgres",
    port="5432"
)

# Создание курсора
cur = conn.cursor()

# Выполнение SQL-запроса
query = """
SELECT subjects.subject_name, exam_schedule.exam_date
FROM exam_schedule
INNER JOIN students ON exam_schedule.student_id = students.id
INNER JOIN subjects ON exam_schedule.subject_id = subjects.id;
"""
cur.execute(query)

# Получение результатов и вывод их на экран
results = cur.fetchall()
for result in results:
    print(f"Предмет: {result[0]}, Дата экзамена: {result[1]}")

# Закрытие соединения с базой данных
cur.close()
conn.close()
