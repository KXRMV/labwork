import os


def convert(num):
    print("Число в восьмеричной системе: ", oct(num))
    print("Число в шестнадцатеричной системе: ", hex(num))


if __name__ == '__main__':
    convert(int(os.environ.get("NUM")))
